import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav = [
  {
    _tag: 'CSidebarNavItem',
    name: 'پیشخوان',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon" />
  }, {
    _tag: 'CSidebarNavTitle',
    _children: ['لینک اصلی']
  }, {
    _tag: 'CSidebarNavItem',
    name: 'صفحه خالی',
    to: '/blank',
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon" />
  }
]

export default _nav
