import React from 'react';

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const Blank = React.lazy(() => import('./views/Blank'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/blank', name: 'Blank', component: Blank }
];

export default routes;
