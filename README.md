# Electron React CoreUI Template

Electron with create react app & coreui version 3 integrated.

## Installation

### Clone repo

``` bash
# clone the repo
$ git clone https://github.com/farzinkakh/electron-react-coreui-boilerplate my-project

# go into app's directory
$ cd my-project

# install app's dependencies
$ npm install

# run the app
$ npm run start
```

### Copy and Paste

Copy all your files to your project folder and then,

``` bash
# go into app's directory
$ cd my-project

# install app's dependencies
$ npm install

# run the app
$ npm run start
```

## Author

**Farzin Askarian Kakh**
* <https://github.com/farzinkakh>
* <https://t.me/farzinkakh>


## Copyright and License

copyright 2022 Farzin Askarian Kakh.   

 
Code released under [the MIT license](https://github.com/farzinkakh/electron-react-coreui-boilerplate/LICENSE).
